/*
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib;

import java.util.Arrays;
import java.util.List;

/**
 * Basic interface for keeping config. This interface just shows the way how we
 * want to read config from file, db etc. Its used for later migration to db
 * sqlite or bigger db system like mysql, postgresql. <b>All implementation should
 * load config in the constructor so we can work with it.</b>
 *
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public interface ConfigManager {

    /**
     * Options as needed by our databases.
     */
    static final List<String> OPTIONS = Arrays.asList("Server", "Port",
            "User", "Password", "LocalPort", "Download_Folder", "Anime_Folder",
            "Extensions", "Auto_Scan", "Pictures_Folder");

    /**
     * Get a saved option as a object, most of them will be string, but you can
     * also store int, Path etc.
     *
     * @param key option name we want to read
     * @return option value in corresponding type. e.g. String or int.
     * @throws IllegalArgumentException Thrown when given option was not given
     * in load Options
     */
    String getOption(String key) throws IllegalArgumentException;

    /**
     * Changes existing option or if option don't yet exists we add new with
     * given value. This function should call {@link #saveConfig() saveConfig()}
     * <b>only</b> when the given option is new and we add it into storage list.
     *
     * @param key option name we want to change or add
     * @param value new value we want to store
     */
    void setOption(String key, String value);

    /**
     * Call we make if there is no loaded options. If we can't load config in
     * loadConfig(); this should be called
     */
    void loadDefaultConfig();

    /**
     * Save the full config into given medium. Will be called on user command.
     */
    void saveConfig();
}
