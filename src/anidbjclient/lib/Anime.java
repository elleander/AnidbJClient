/*
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib;

import anidbjclient.lib.anidb.AnidbException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class storing info about selected anime entry in db. There is no update
 * functions in this class all the info stored will be 'final' if you made some
 * changes in the DB you will need to create new instance of this class for that
 * selected anime.
 *
 * @author Krzysztof Troska <elleander86 at gmail.com>
 */
public final class Anime {

    private final int aid;
    private final String name, type, picName, dateFlags;
    private String description;
    private final boolean adult;
    private final int airDate, endDate, episodes, ownedEpisodes;
    private Path pic = null;
    private static Path picDir = null;
    private final boolean isWatched;

    public Anime(ResultSet rs) throws SQLException {
        aid = rs.getInt("aid");
        name = rs.getString("name");
        episodes = rs.getInt("episodes");
        type = rs.getString("anime_type");
        adult = rs.getBoolean("adult");
        dateFlags = rs.getString("date_flag");
        airDate = rs.getInt("air_date");
        endDate = rs.getInt("end_date");
        picName = rs.getString("pic_name");
        isWatched = rs.getBoolean("watched");
        ownedEpisodes = rs.getInt("owned_episodes");
        description = "";
        if (picDir == null) {
            this.setPicDir();
        }
    }

    private void setPicDir() {
        ConfigManagerAnidbJClient cm = new ConfigManagerAnidbJClient(new ConfigManagerFile());
        try {
            picDir = cm.getPicPath();
        } catch (IOException ex) {
            Logger.getLogger(Anime.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void downloadPic() throws IOException {
        if (picDir == null) {
            throw new IOException("Missing pictures folder check your config.");
        }
        pic = Paths.get(picDir.toString(), picName);
        if (pic.toFile().isFile()) {
            return;
        }
        URL website = new URL("http://img7.anidb.net/pics/anime/" + picName);
        Files.copy(website.openStream(), pic);
    }

    public int getAid() {
        return aid;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getDateFlabs() {
        return dateFlags;
    }

    public int getAirDate() {
        return airDate;
    }

    public int getEndDate() {
        return endDate;
    }

    public int getEpidoes() {
        return episodes;
    }

    public Path getPic() throws IOException {
        if (pic != null) {
            return pic;
        }
        this.downloadPic();
        return pic;
    }

    public int getOwnedEpisodes() {
        return ownedEpisodes;
    }

    public String getDescription() throws SQLException, AnidbException {
        if (description.isEmpty()) {
            DatabaseSQLite db = new DatabaseSQLite();
            description = db.getAnimeDescription(aid);
        }
        return description;
    }

    public boolean isWatched() {
        return isWatched;
    }

    @Override
    public String toString() {
        if (name.isEmpty()) {
            return String.format("Anime id " + aid);
        } else {
            return "Anime id " + aid + ", Name: " + name;
        }
    }
}
