/* 
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles interface {@link anidbjclient.lib.ConfigManager} while storing all
 * options in a text file.
 *
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public class ConfigManagerFile implements ConfigManager {

    /**
     * Map storing all config values.
     */
    private static final Map<String, String> CONFIG = Collections.synchronizedMap(
            new HashMap<>());

    /**
     * Path to a config file.
     */
    private static final Path PATH = Paths.get(new File("etc/main.conf").getAbsolutePath());

    /**
     * Puts default options into maps
     */
    public ConfigManagerFile() {
        ConfigManager.OPTIONS.stream().forEach((option) -> {
            CONFIG.put(option, null);
        });
        this.loadConfig();
    }

    /**
     * Loads config from file
     */
    private void loadConfig() {
        /**
         * Check if config file exist if not we try to create default one TODO:
         * make it throw IOException and call some option creation app
         */
        List<String> lines;
        try {
            lines = Files.readAllLines(PATH, Charset.defaultCharset());
        } catch (NoSuchFileException e) {
            System.err.println("Config File don't exist loading default values.");
            this.loadDefaultConfig();
            //this.saveConfigToFile();
            return;
        } catch (IOException ex) {
            System.err.println("Can't read from confing file.");
            return;
        }
        lines.stream().forEach((line) -> {
            this.readLine(line);
        });
    }

    /**
     * Reads line of config file into CONFIG map, drops line when a comment or
     * has more than one =
     *
     * @param line
     */
    private void readLine(String line) {
        if (line.matches("^##.*")) {
            return; //comment
        } else if (!line.matches("^[a-zA-z_]+=[^=]+$")) {
            System.err.format("Line: %s\n Is not vaild configuration file line.\n", line);
            return;
        }
        /**
         * Small waste of memory but it makes code more readable
         */
        String tmp[] = line.split("=");
        String key = tmp[0];
        String value = tmp[1];
        /**
         * Check if this key value is in options, if not we drop it silently
         */
        if (CONFIG.containsKey(key)) {
            CONFIG.put(key, value); // put new or update
        }
    }

    /**
     * Returns option by given key (option name). Remember that option is not
     * checked in any way.
     *
     * @param key - name of a option
     * @return option for given key or null
     */
    @Override
    public String getOption(String key) throws IllegalArgumentException {
        if (CONFIG.containsKey(key)) {
            return CONFIG.get(key);
        } else {
            throw new IllegalArgumentException("No such option stored.");
        }
    }

    /**
     * Changes existing option or if option don't yet exists we add new with
     * given value. This function should call {@link #saveConfig() saveConfig()}
     * <b>only</b> when the given option is new and we add it into storage list.
     *
     * @param key option name we want to change or add
     * @param value new value we want to store
     */
    @Override
    public void setOption(String key, String value) {
        if (CONFIG.containsKey(key)) {
            CONFIG.put(key, value);
        } else {
            CONFIG.put(key, value);
            this.saveConfig();
        }
    }

    /**
     * Overwrites config map with default values.
     */
    @Override
    public void loadDefaultConfig() {
        Map<String, String> map = new HashMap<>();
        map.put("Server", "api.anidb.net");
        map.put("Port", "9000");
        map.put("LocalPort", "22356");
        map.put("Download_Folder", Paths.get(System.getProperty("user.home"), "Downloads").toString());
        map.put("Anime_Folder", Paths.get(System.getProperty("user.home"), "Videos", "Anime").toString());
        map.put("Extensions", "!qB|!ut");
        map.put("User", "user_name");
        map.put("Password", "password");
        map.put("Auto_Scan", "false");
        map.put("Pictures_Folder", Paths.get(new File(".").getAbsolutePath(), "pics").toString());
        CONFIG.putAll(map);
    }

    /**
     * Saves actually stored config into a file.
     */
    @Override
    public synchronized void saveConfig() {
        try {
            this.createConfigFile();
        } catch (IOException e) {
            System.err.println("Cant create config file etc/main.conf.");
            System.exit(0);
        }
        /* Now save info in file */
        try (PrintWriter out = new PrintWriter(PATH.toFile())) {
            out.println("##Config File for anidbJclient you want to change user,"
                    + " password, Download and Anime folder.");
            out.println("##Any of the lines in config can contain only one = "
                    + "(you can't use = in user_name or password).");
            out.println("##Boolean options should be set either true or false NO"
                    + " on off etc.).");
            CONFIG.entrySet().stream().forEach((entry) -> {
                out.println(entry.getKey() + "=" + entry.getValue());
            });
        } catch (IOException e) {
            System.err.println("Cant create config file etc/main.conf.");
        }
    }

    /**
     * Create default config file, if it exists saves backup adding ~ to the the
     * file name.
     *
     * @throws IOException If can't read from file or file don't exist.
     */
    private void createConfigFile() throws IOException {
        Path dir = PATH.getParent();
        if (PATH.toFile().exists()) {
            Files.move(PATH, Paths.get(dir.toString(), "~" + PATH.getFileName().toString()),
                    StandardCopyOption.REPLACE_EXISTING);
        }
        if (!dir.toFile().isDirectory() && !dir.toFile().mkdirs()) {
            throw new IOException("Can't create directory: " + dir.toString());
        }
        PATH.toFile().createNewFile();
    }
}
