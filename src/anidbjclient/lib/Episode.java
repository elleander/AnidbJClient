/* 
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib;

import anidbjclient.bin.AnidbDaemon;
import anidbjclient.lib.anidb.AnidbException;
import anidbjclient.lib.anidb.AnidbMissingContentException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles files and directories knows what to do. Although its called episode
 * eid should <b>NOT</b> be used for reference, only a fid. because somebody can
 * have same episode from 2 different sub groups.
 *
 * @author Krzysztof Troska <elleander86 at gmail.com>
 */
public final class Episode {

    /**
     * Storing file.
     */
    private Path episode;
    private String hash;
    private int fid, aid, eid;
    private EpisodeStatus status;
    Anime anime; // parent?

    public Episode(Path episode) {
        this.episode = episode;
    }

    public Episode(Path path, String hash) {
        episode = path;
        this.hash = hash;
        status = EpisodeStatus.HASHED;
    }

    public Episode(Path path, String hash, int fid) {
        episode = path;
        this.hash = hash;
        this.fid = fid;
        status = EpisodeStatus.IN_DB;
    }

    public Episode(ResultSet rs) throws SQLException {
        this.episode = Paths.get(rs.getString("path"));
        this.fid = rs.getInt("fid");
        this.aid = rs.getInt("aid");
        this.eid = rs.getInt("eid");
        this.status = EpisodeStatus.STORED;
    }

    public EpisodeStatus getStatus() {
        return status;
    }

    /**
     * Load file into Episode class for further work.
     *
     * @param episode to a file
     * @throws IllegalArgumentException not a path or file do not exists.
     */
    public void setFile(Path episode) throws IllegalArgumentException {
        if (!episode.toFile().isFile()) {
            throw new IllegalArgumentException("Given file is not a real file.");
        }
        this.episode = episode;
        fid = -1;
        aid = -1;
        eid = -1;
        this.status = EpisodeStatus.UNKNOWN;
    }

    /**
     * Returns true if file should be checked. first we check if FORCE_CHECK is
     * passed next we check if file is in files to add with valid fid next
     * extension from ConfigManagerFile, next if file is really video last if
     * file is in other_files db
     *
     * @return new status of the episode.
     * @throws java.sql.SQLException when local db error we can't check file.
     * @throws java.io.IOException when we can't read from file. Its needed if
     * we want to count ed2k Hash.
     * @throws anidbjclient.lib.anidb.AnidbException
     */
    public EpisodeStatus updateStatus() throws SQLException, IOException,
            AnidbException {
        DatabaseSQLite db = new DatabaseSQLite();
        switch (status) {
            case UNKNOWN:
                if (db.checkOtherFiles(episode)) {
                    status = EpisodeStatus.OTHER; // not an anime
                    return status;
                }
                countHash();
                status = EpisodeStatus.HASHED;
            case NOT_ANIDB:
            case OTHER:
                return status; // file is already in other files nothing to do
            case HASHED:
                try {
                    fid = db.checkFile(hash);
                    if (fid == -1) {
                        db.processNewFile(this);
                    }
                    status = EpisodeStatus.STORED;
                } catch (AnidbMissingContentException ex) {
                    Logger.getLogger(Episode.class.getName()).log(Level.SEVERE, null, ex);
                    status = EpisodeStatus.NOT_ANIDB;
                }
            case IN_DB:
                if (moveFile()) {
                    status = EpisodeStatus.STORED;
                }
                return status;
            case STORED:
                return status;
            case DELETED:
                //TODO: here we will delete this file.
                throw new UnsupportedOperationException("Not supported yet.");
            default:
                return status;
        }
    }

    public String countHash() throws IOException {
        Ed2kHash ed2k = new Ed2kHash(episode.toFile());
        hash = ed2k.getHash();
        status = EpisodeStatus.HASHED;
        return hash;
    }

    /**
     * Send given path to daemon process so it will be checked outside of queue.
     *
     * @param path path to a file or directory
     */
    public static void addFile(Path path) {
        AnidbDaemon.filesToAdd.add(path);
    }

    public boolean moveFile(Anime anime) {
        this.anime = anime;
        return moveFile();
    }
    
    public String getHash() throws IOException {
        if (status != EpisodeStatus.HASHED) {
            countHash();
        }
        return hash;
    }

    /**
     * TODO: Change to temp database than move and than force changes.
     * @return 
     */
    public boolean moveFile() {
        if (anime == null || episode == null) {
            throw new IllegalArgumentException("No anime or file given.");
        }
        ConfigManagerAnidbJClient cm = new ConfigManagerAnidbJClient(new ConfigManagerFile());
        String animeName = changeName(anime.getName());
        Path dest = Paths.get(cm.getAnimePath().toString());
        dest = makePath(dest, anime.getType(), anime.isAdult());
        dest = Paths.get(dest.toString(), animeName);
        dest.toFile().mkdirs();
        dest = Paths.get(dest.toString(), episode.getFileName().toString());
        try {
            Files.move(episode, dest, StandardCopyOption.REPLACE_EXISTING);
            try {
                DatabaseSQLite db = new DatabaseSQLite();
                db.updateEpisodePath(dest, fid);
            } catch (SQLException ex) {
                Logger.getLogger(Episode.class.getName()).log(Level.SEVERE, null, ex);
                // can't update file path in db so we move file back. 
                Files.move(dest, episode, StandardCopyOption.REPLACE_EXISTING);
                return false;
            }
            return true;
        } catch (IOException ignore) {
            return false;
        }
    }

    /**
     * Changes name so it wont be refused by OS path. Tested for windows right 
     * now.
     *
     * @param name to be changed
     * @return changed name
     */
    private String changeName(String name) {
        String[][] replacements = {{"<", ""},
        {">", ""}, {"/", "-"}, {"\\", "/"}, {":", "-"},
        {"*", ""}, {"?", "."}, {"\"", "'"}, {"|", ""}};
        for (String[] replacement : replacements) {
            name = name.replace(replacement[0], replacement[1]);
        }
        name = name.trim();
        while (name.matches(".*\\.+$")) {
            name = name.substring(0, name.length() - 1);
        }
        return name;
    }

    /**
     * Creates path for a file to be moved.
     *
     * @param path path to the file to be moved
     * @param type anime type
     * @param adult boolean if anime 18+
     * @return path to destination without file name
     */
    private Path makePath(Path path, String type, boolean adult) {
        if (adult) {
            return Paths.get(path.toString(), "H");
        }
        if (type.matches("TV Series")) {
            return path;
        } else {
            return Paths.get(path.toString(), type);
        }
    }

    /**
     * Prints path to a file with on which we are working now. If there is no
     * path set we return standard toString output.
     *
     * @return
     */
    @Override
    public String toString() {
        if (this.episode == null) {
            return getClass().getName() + "@" + Integer.toHexString(hashCode());
        } else {
            return this.episode.toString();
        }
    }
}
