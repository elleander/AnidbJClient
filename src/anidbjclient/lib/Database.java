/*
 * Copyright (C) 2015 Krzysztof Troska (elleander86 at gmail.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib;

import java.sql.SQLException;
import java.util.Map;

/**
 * Interface for a Database, main purpose is creating and deleting info about 
 * locally stored files and cached info from anidb. Most of the info should be 
 * stored in memory and updated in as soon as we got new info about file so we 
 * won't lose any info if <em>application</em> crashes. Connection should be 
 * transparent and no special calls should be made.
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public interface Database {
    /**
     * Inserts file into local database, should also be ready to update if file
     * is already there.
     * @param episode episode we want to update info about
     * @return true on success *****TEMP*****
     * @throws SQLException when we got database error
     */
    boolean insertFile(Episode episode) throws SQLException;
    
    /**
     * Inserts anime info into local database, should also be ready to update it
     * if anime is already in list.
     * @param anime
     * @return
     * @throws SQLException 
     */
    boolean insertAnime(Anime anime) throws SQLException;
    
    /**
     * Return episode instance from local db.
     * @param fid file id for of a returned episode.
     * @return
     * @throws SQLException 
     */
    Episode getEpisode(int fid) throws SQLException;
    
    /**
     * Returns anime instance for given aid.
     * @param aid
     * @return
     * @throws SQLException 
     */
    Anime getAnime(int aid) throws SQLException;
    
    /**
     * Returns all episodes in local Database for a given anime id.
     * @param aid
     * @return
     * @throws SQLException 
     */
    Map<Integer, Episode> getEpisodesMap(int aid) throws SQLException;
    
    /**
     * Returns all anime we got info about in local db. as aid and anime
     * instance.
     * @param aid 
     * @return
     * @throws SQLException 
     */
    Map<Integer, Anime> getAnimeMap(int aid) throws SQLException;
    
}
