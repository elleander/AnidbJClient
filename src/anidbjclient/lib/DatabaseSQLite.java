/* 
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib;

import anidbjclient.lib.anidb.AnidbException;
import anidbjclient.lib.anidb.AnidbHandler;
import anidbjclient.lib.anidb.AnidbMissingContentException;
import anidbjclient.lib.anidb.AnidbOfflineException;
import anidbjclient.lib.sql.Convert;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles all data, extends AnidbHandler from witch we get anidb info if needed
 *
 * @author Krzysztof Troska <elleander86 at gmail.com>
 */
public class DatabaseSQLite implements Database {

    /**
     * Basic db driver (sqlite).
     */
    public static final String DRIVER = "org.sqlite.JDBC";
    
    /**
     * Link to db sqlite type, ani.db file.
     */
    public static final String DB_URL = "jdbc:sqlite:ani.db";

    /**
     * creates AnidbHandler instance - we use that a lot
     */
    private static final AnidbHandler ah = new AnidbHandler(new ConfigManagerAnidbJClient(new ConfigManagerFile()));
    
    /**
     * Connection to local database we declare it static but you still need to
     * call constructor to get connection.
     */
    private static Connection conn = null;

    /**
     * Creates connection with local db, if connection can't be established we
     * throw exception.
     * @throws java.sql.SQLException when there is db creating problem.
     */
    public DatabaseSQLite() throws SQLException {
        try {
            Class.forName(DatabaseSQLite.DRIVER);
        } catch (ClassNotFoundException e) {
            throw new SQLException ("No Driver for sqlite");
        }
        if (conn == null) {
            conn = DriverManager.getConnection(DB_URL);
            /**
             * If we can't create default db we want to try close the connection.
             */
            try {
                createDatabase();
            } catch (SQLException e) {
                if(conn != null) {
                    conn.close();
                    conn = null;
                }
                throw new SQLException(e.getMessage(), e);
            }
        }
    }

    /**
     * Creates local tables for later use, we DO NOT use foreign key because
     * *reasons*.
     *
     * @return true on success
     */
    private void createDatabase() throws SQLException {
        String anime = "CREATE TABLE IF NOT EXISTS anime (aid INTEGER PRIMARY KEY, name STRING, episodes INTEGER, anime_type STRING, adult BOOLEAN, "
                + "date_flag STRING, year STRING, air_date INTEGER, end_date INTEGER, pic_name STRING);";
        String files = "CREATE TABLE IF NOT EXISTS files (fid INTEGER PRIMARY KEY, aid INTEGER, eid INTEGER, gid INTEGER, "
                + "size INTEGER, ed2k STRING, filename STRING, path STRING);";
        String groups = "CREATE TABLE IF NOT EXISTS groups (gid INTEGER PRIMARY KEY, name STRING);";
        String other_files = "CREATE TABLE IF NOT EXISTS other_files (ed2k STRING UNIQUE, path STRING);";
        String files_to_add = "CREATE TABLE IF NOT EXISTS files_to_add (ed2k STRING, path String UNIQUE, fid INTEGER);";
        String description = "CREATE TABLE IF NOT EXISTS description (aid INTEGER UNIQUE, description TEXT);";
        String myList = "CREATE TABLE IF NOT EXISTS my_list (my_list_id INTEGER PRIMARY KEY, fid INTEGER, my_list_state INTEGER, "
                + "my_list_file_state INTEGER, viewed_date INTEGER);";
        String episodes = "CREATE TABLE IF NOT EXISTS episodes (eid INTEGER PRIMARY KEY, aid INTEGER, air_date INTEGER, ep_number "
                + "STRING, type INTEGER, ep_name STRING, ep_name_en STRING);";
        try (Statement stat = conn.createStatement()) {
            stat.execute(anime);
            stat.execute(files);
            stat.execute(groups);
            stat.execute(other_files);
            stat.execute(files_to_add);
            stat.execute(description);
            stat.execute(myList);
            stat.execute(episodes);
        }
    }

    /**
     * Returns fid if file is already in the db, or -1
     * 
     * @param hash
     * @return fid if file inside db -1 if not inside
     * @throws SQLException when we can't read data from db.
     */
    @SuppressWarnings("empty-statement")
    public int checkFile(String hash) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT "
                + "COUNT(*) AS total, fid FROM files WHERE ed2k= ?;")) {
            prepStmt.setString(1, hash);
            rs = prepStmt.executeQuery();
            rs.next();
            if (rs.getInt("total") > 0) {
                return rs.getInt("fid");
            } else
                return -1;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }
    
    /**
     * Returns fid if file is already in the db, or -1
     * 
     * @param path to a file we want to check if its not already in db.
     * @return fid if file inside db or -1 if not found
     * @throws SQLException when we can't read data from db.
     */
    @SuppressWarnings("empty-statement")
    public int checkFile(Path path) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT COUNT(*) AS total, fid FROM files WHERE path= ?;")) {
            prepStmt.setString(1, path.toString());
            rs = prepStmt.executeQuery();
            rs.next();
            if (rs.getInt("total") > 0) {
                return rs.getInt("fid");
            } else
                return -1;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }
    
    /**
     * Handles new file check if its anidb file we download all info about it
     * and put it in local db than return fid.
     *
     * @param episode episode instance we want to add
     * @return fid if success and -1 on error
     * @throws java.sql.SQLException when local db error
     * @throws anidbjclient.lib.anidb.AnidbMissingContentException
     * @throws anidbjclient.lib.anidb.AnidbOfflineException
     * @throws java.io.IOException
     */
    @SuppressWarnings("empty-statement")
    public int processNewFile(Episode episode) throws AnidbException, 
            AnidbMissingContentException, AnidbOfflineException, SQLException, IOException {
        if(episode.getStatus() != EpisodeStatus.HASHED)
            throw new IllegalArgumentException("Can't add this file, already in"
                    + " or not anidb file");
        String[] tmp;
        tmp = ah.importFileInfo(episode.getPath().getFileName(), episode.getHash());
        
        int fid;
        try {
            fid = Integer.parseInt(tmp[0]);
        } catch (NumberFormatException ex) {
            throw new AnidbException("Can't get int from fid. tmp[0]" + tmp[0]);
        }
        insertGroup(tmp);

        try {
            int aid = Integer.parseInt(tmp[1]);
            insertAnime(aid);
            insertAnimeDescription(aid);
        } catch (NumberFormatException ex) {
            throw new AnidbException("Can't get int from aid. tmp[1]" + tmp[1]);
        }

        try {
            insertEpisode(Integer.parseInt(tmp[2]));
        } catch (NumberFormatException ex) {
            throw new AnidbException("Can't get int from gid. tmp[2]" + tmp[2]);
        }
        
        insertFile(episode.getPath().toFile(), tmp);
        
        try {
            if (Integer.parseInt(tmp[4]) > 0) {
                insertMyList(tmp);
            } else {
                insertMyList(fid);
            }
        } catch (NumberFormatException ex) {
            /*
             * we insert new info in case anidb returned wrong number in tmp[4]
             */
            insertMyList(fid);
            throw new AnidbException("Can't get int from myListId. tmp[4]: " + fid);
        }
        return fid;
    }

    @SuppressWarnings("empty-statement")
    public String getAnimeDescription(int aid) throws SQLException, AnidbException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT description"
                + " FROM description WHERE aid= ? ;")) {
            prepStmt.setInt(1, aid);
            rs = prepStmt.executeQuery();
            try {
                return rs.getString("description");
            } catch (SQLException ex) {
                return this.insertAnimeDescription(aid);
            }
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Updates episode path in local DB.
     *
     * @param path - NEW path to file
     * @param fid id of the file we want to change path
     * @throws java.sql.SQLException
     */
    public void updateEpisodePath(Path path, int fid) throws SQLException {
        try (PreparedStatement prepStmt = conn.prepareStatement("UPDATE files SET"
                + " path = ? WHERE fid = ?;")) {
            prepStmt.setString(1, path.toString());
            prepStmt.setInt(2, fid);
            prepStmt.execute();
        }
    }

    /**
     * Check by path if file is in other files db its should be used BEFORE
     * counting ed2k hash.
     *
     * @param path path to a file for check
     * @return true if file is inside other files other cases false
     * @throws java.sql.SQLException
     */
    @SuppressWarnings("empty-statement")
    public boolean checkOtherFiles(Path path) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT COUNT(*) AS total FROM other_files WHERE path = ?;")) {
            prepStmt.setString(1, path.toString());
            rs = prepStmt.executeQuery();
            return rs.getInt("total") > 0;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Check by path if file is in files_to_add db its should be used BEFORE
     * counting ed2k hash
     *
     * @param path path to a file for check
     * @return true if file is in files_to_add
     * @throws java.sql.SQLException
     */
    @SuppressWarnings("empty-statement")
    public boolean checkFilesToAdd(Path path) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT COUNT(*)"
                + " AS total FROM files_to_add WHERE path = ?;")) {
            prepStmt.setString(1, path.toString());
            rs = prepStmt.executeQuery();
            return (rs.getInt("total") > 0);
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Add file to my list in Anidb with status on HDD and watched at given time
     *
     * @param fid file id
     * @viewTime time in Unix time stamp of watch
     * @return true if success
     */
    private boolean insertMyList(int fid, long viewTime) throws SQLException, 
            AnidbException {
        int myListId = getMyListId(fid);
        if (myListId > 0) {
            return true;
        }
        try (PreparedStatement prepStmt = conn.prepareStatement("INSERT INTO "
                + "my_list VALUES (?, ?, ?, ?, ?);")) {
            myListId = ah.addToMyList(fid, -1);
            /*
             * Prepere stmt to add default values into my_list db
             */
            prepStmt.setInt(1, myListId);
            prepStmt.setInt(2, fid);
            prepStmt.setInt(3, 1);
            prepStmt.setInt(4, 1);
            prepStmt.setLong(5, viewTime);
            prepStmt.execute();
            return true;
        }
    }
    
   /**
     * Returns my list id for given fid
     *
     * @param fid file id
     * @return my list id of given file
     * @throws java.sql.SQLException
     */
    @SuppressWarnings("empty-statement")
    public int getMyListId(int fid) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT my_list_id"
                + " FROM my_list WHERE fid = ?;")) {
            prepStmt.setInt(1, fid);
            rs = prepStmt.executeQuery();
            rs.next();
            return rs.getInt("my_list_id");
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }
    
    /**
     * Add file to my list in Anidb with default values not watched and status
     * on HDD.
     *
     * @param fid - file id
     * @return true if success
     */
    private boolean insertMyList(int fid) throws SQLException, AnidbException {
        return insertMyList(fid, -1);
    }

    /**
     * Inserts my list info into db (file was already added to anidb)
     * @param arg look code
     * @throws SQLException when we can't communicate with local db. 
     */
    private void insertMyList(String[] arg) throws SQLException {
        try (PreparedStatement prepStmt = conn.prepareStatement("INSERT OR REPLACE INTO my_list VALUES (?, ?, ?, ?, ?);")) {
            prepStmt.setString(1, arg[4]); // my list id
            prepStmt.setString(2, arg[0]); // file id
            prepStmt.setString(3, arg[6]); // my list state
            prepStmt.setString(4, arg[7]); // my list file state
            try {
                int viewDate = Integer.parseInt(arg[8]);
                if (viewDate > 0) {
                    prepStmt.setInt(5, viewDate);
                } else {
                    prepStmt.setInt(5, -1);
                }
            } catch (NumberFormatException e) {
                prepStmt.setInt(5, -1);
            }
            prepStmt.execute();
        }
    }

    /**
     * Insert file into local db
     * @param file file to be inserted
     * @param arg array of strings in proper order (look
     * @throws SQLException when local db error.
     */
    private void insertFile(File file, String arg[]) throws SQLException {
        try (PreparedStatement prepStmt = conn.prepareStatement("INSERT OR REPLACE "
                + "INTO files VALUES (?, ?, ?, ?, ?, ?, ?, ?);")) {
            prepStmt.setInt(1, Integer.parseInt(arg[0])); //file id
            prepStmt.setInt(2, Integer.parseInt(arg[1])); //anime id
            prepStmt.setInt(3, Integer.parseInt(arg[2])); //episode id
            prepStmt.setInt(4, Integer.parseInt(arg[3])); //group id
            prepStmt.setLong(5, file.length());
            prepStmt.setString(6, arg[10]); // ed2k hash
            prepStmt.setString(7, file.getName());
            prepStmt.setString(8, file.getAbsolutePath());
            prepStmt.execute();
        }
    }

    /**
     * Insert new group into local db - no contact with anidb
     *
     * @param arg array of strings in proper order (look
     * AnidbHandler.importFileInfo())
     * @return true if success
     */
    @SuppressWarnings("empty-statement")
    private void insertGroup(String arg[]) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("INSERT OR REPLACE INTO groups VALUES (?, ?);")) {
            prepStmt.setInt(1, Integer.parseInt(arg[3])); //group id
            prepStmt.setString(2, arg[9]); //group name
            prepStmt.execute();
        } catch (NumberFormatException ex) {
            throw new SQLException("Wrong group id in insertGroup(String[])");
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Insert new group into local db, it makes use of data from AnidbHandler
     * EPISODE API.
     *
     * @param gid id of a group
     * @return true if success
     */
    @SuppressWarnings("empty-statement")
    private boolean insertGroup(int gid) throws SQLException, AnidbException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("INSERT OR "
                + "REPLACE INTO groups VALUES (?, ?);")) {
            String[] tmp = ah.importGroupInfo(gid);
            prepStmt.setString(1, tmp[0]); //group id
            prepStmt.setString(2, tmp[5]); //group name
            prepStmt.execute();
            return true;
        } catch (NumberFormatException ex) {
            throw new AnidbException("Wrong group id in insertGroup(int)");
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Insert new anime title into local db. Downloads new infor from anidb if
     * there is no cached option in local db. <br />Throws exception acording to
     * the error we got when failed to get data.
     *
     * @param aid id of a anime we want to add to local db.
     * @return true if success
     * @throws java.sql.SQLException
     * @throws anidbjclient.lib.anidb.AnidbMissingContentException
     */
    @SuppressWarnings("empty-statement")
    public final boolean insertAnime(int aid) throws SQLException, 
            AnidbMissingContentException, AnidbException {
        ResultSet rs = null;
        String[] arg;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT COUNT(*)"
                + " AS total FROM anime WHERE aid = ?;")){
            prepStmt.setInt(1, aid);
            rs = prepStmt.executeQuery();
            if (rs.getInt("total") > 0) {
                return true;
            }
            arg = ah.importAnimeInfo(aid);
        } finally {
            try {if( rs != null) rs.close(); } catch (SQLException e) {} ;
        }
        try (PreparedStatement prepStmt = conn.prepareStatement("INSERT INTO "
                + "anime VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")) {
            prepStmt.setInt(1, Integer.parseInt(arg[0])); // aid
            prepStmt.setString(2, arg[4]); //name
            prepStmt.setInt(3, Integer.parseInt(arg[5])); //episodes
            prepStmt.setString(4, arg[3]); //type
            if (Integer.parseInt(arg[9]) == 1) {
                prepStmt.setBoolean(5, Boolean.TRUE); //adult
            } else {
                prepStmt.setBoolean(5, Boolean.FALSE); //adult
            }
            prepStmt.setInt(6, Integer.parseInt(arg[1])); //date flags
            prepStmt.setString(7, arg[2]); //year
            prepStmt.setInt(8, Integer.parseInt(arg[6])); // air date
            prepStmt.setInt(9, Integer.parseInt(arg[7])); // end date
            prepStmt.setString(10, arg[8]); // pic name
            prepStmt.execute();
            Anime anime = getAnime(Integer.parseInt(arg[0]));
            anime.downloadPic();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(DatabaseSQLite.class.getName()).log(Level.SEVERE, null, ex);
            return true; // no image but still good
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Insert new episode info into local db
     *
     * @param arg array of strings in proper order (look
     * AnidbHandler.importFileInfo())
     */
    @SuppressWarnings("empty-statement")
    private void insertEpisode(int eid) throws SQLException, 
            AnidbMissingContentException, AnidbException {
        String[] arg;
        PreparedStatement prepStmt = null;
        ResultSet rs = null;
        try {
            prepStmt = conn.prepareStatement("SELECT COUNT(*) AS total FROM episodes"
                    + " WHERE eid = ?;");
            prepStmt.setInt(1, eid);
            rs = prepStmt.executeQuery();
            if (rs.getInt("total") > 0) {
                return;
            }
            arg = ah.importEpisodeInfo(eid);
            prepStmt = conn.prepareStatement("INSERT INTO episodes VALUES (?, ?, ?, ?, ?, ?, ?);");
            prepStmt.setInt(1, Integer.parseInt(arg[0])); //eid
            prepStmt.setInt(2, Integer.parseInt(arg[1])); //aid
            prepStmt.setInt(3, Integer.parseInt(arg[9])); //air_date
            /**
             * Change ep number into normal int
             */
            int type = Integer.parseInt(arg[10]);
            char typeChar = getSpecialChar(type);
            String epNumber = arg[5];
            if (!Character.isWhitespace(typeChar)) {
                epNumber = epNumber.replaceAll("[A-Z]", "");
            }
            prepStmt.setString(4, epNumber);
            prepStmt.setInt(5, type); //type
            prepStmt.setString(6, arg[7]); //ep_name
            prepStmt.setString(7, arg[6]); //ep_name_en
            prepStmt.execute();
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
            try { if (prepStmt != null) prepStmt.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Returns special name used as anidb episode number prefix
     *
     * @param type type number from 1-6
     * @return character according to type or space
     */
    public String getSpecialString(int type) {
        switch (type) {
            case 1:
                return "Regular Episode"; // regular episode no prefix
            case 2:
                return "Special"; // special 
            case 3:
                return "Credit"; // credit
            case 4:
                return "Trailer"; // trailer
            case 5:
                return "Parody"; // parody
            case 6:
                return "Other"; // other
            default:
                return ""; // should not happend
        }
    }

    /**
     * Returns special character used as anidb episode nr prefix
     *
     * @param type type number from 1-6
     * @return character according to type or space
     */
    private char getSpecialChar(int type) {
        switch (type) {
            case 1:
                return ' '; // regular episode no prefix
            case 2:
                return 'S'; // special 
            case 3:
                return 'C'; // credit
            case 4:
                return 'T'; // trailer
            case 5:
                return 'P'; // parody
            case 6:
                return 'O'; // other
            default:
                return ' '; // should not happend
        }
    }

    /**
     * Inserts file into other_files db so we won't have to count hash of this
     * file many times
     *
     * @param path path to file
     * @param hash ed2k hash
     * @throws java.sql.SQLException
     */
    @SuppressWarnings("empty-statement")
    public void insertOtherFiles(final Path path, final String hash) 
            throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("INSERT OR "
                + "REPLACE INTO other_files VALUES (?, ?);")) {
            prepStmt.setString(1, hash); //ed2k hash
            prepStmt.setString(2, path.toString());
            prepStmt.execute();
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Inserts or updates file into files_to_add db so we can add them to db
     * when anidb is available
     *
     * @param path to file
     * @param hash ed2k hash
     * @param fid id of a file in anidb
     * @return true if success or already inside
     * @throws java.sql.SQLException
     */
    @SuppressWarnings("empty-statement")
    public boolean insertFilesToAdd(Path path, String hash, int fid) 
            throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("INSERT OR REPLACE INTO files_to_add VALUES (?, ?, ?);");) {
            prepStmt.setString(1, hash);
            prepStmt.setString(2, path.toString());
            prepStmt.setInt(3, fid);
            prepStmt.execute();
            return true;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Inserts or updates file into files_to_add db so we can add them to db
     * when anidb is available, sets fid to default -1 value so we know this file
     * was not tested yet
     *
     * @param path to file
     * @param hash ed2k hash
     * @return true if success or already inside
     * @throws java.sql.SQLException
     */
    public boolean insertFilesToAdd(Path path, String hash) throws SQLException {
        return insertFilesToAdd(path, hash, -1);
    }

    /**
     * Inserts or updates file into files_to_add db so we can add them to db
     * when anidb is available, sets fid to default -1 and hash to 0 so we know
     * this file was not tested yet.
     *
     * @param path to file
     * @return true if success or already inside
     * @throws java.sql.SQLException
     */
    public boolean insertFilesToAdd(Path path) throws SQLException {
        return insertFilesToAdd(path, "0", -1);
    }

    /**
     * Set current time as watch time for a given fid
     *
     * @param fid id of a file we want to mark watched
     * @throws java.sql.SQLException
     * @throws anidbjclient.lib.anidb.AnidbException
     */
    @SuppressWarnings("empty-statement")
    public void markWatched(int fid) throws SQLException, AnidbException {
        ResultSet rs = null;
        PreparedStatement prepStmt = null;
        long viewTime = System.currentTimeMillis() / 1000;
        try {
            prepStmt = conn.prepareStatement("SELECT COUNT(*) AS "
                    + "total, viewed_date, my_list_id FROM my_list WHERE fid = ?;");
            prepStmt.setInt(1, fid);
            rs = prepStmt.executeQuery();
            if (rs.getInt("total") == 0)
                insertMyList(fid, viewTime);
            int lid = rs.getInt("my_list_id");
            prepStmt = conn.prepareStatement("UPDATE my_list SET viewed_date = ? "
                    + "WHERE fid = ?;");
            prepStmt.setLong(1, viewTime);
            prepStmt.setInt(2, fid);
            /*
             * updating anidb and than we update local db so we wont get
             * different values.
             */
            ah.editMyList(lid, 1, viewTime);
            prepStmt.execute();
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
            try { if (prepStmt != null) prepStmt.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Downloads all missing entries in databases my list, anime, group,
     * episodes.
     *
     * @throws anidbjclient.lib.anidb.AnidbException
     * @throws java.sql.SQLException
     */
    public void repairDatabase() throws AnidbException, SQLException {
        repairMyList();
        repairAnime();
        repairEpisodes();
        repairGroup();
        repairDescription();
    }

    /**
     * Adds ALL missing files to my list
     *
     */
    @SuppressWarnings("empty-statement")
    private void repairMyList() throws AnidbException, SQLException {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT fid FROM files WHERE fid NOT IN (SELECT files.fid "
                    + "FROM files, my_list WHERE files.fid = my_list.fid);");
            while (rs.next())
                insertMyList(rs.getInt("fid"));
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
            try { if (stmt != null) stmt.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Adds ALL missing descriprions to my list
     */
    @SuppressWarnings("empty-statement")
    private void repairDescription() throws SQLException, AnidbException {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT aid FROM anime WHERE aid NOT IN "
                    + "(SELECT aid FROM description);");
            while (rs.next()) {
                insertAnimeDescription(rs.getInt("aid"));
            }
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
            try { if (stmt != null) stmt.close(); } catch (SQLException e) {};
        }
    }
    
    /**
     * Adds all missing anime informations
     *
     * @return true if success
     */
    @SuppressWarnings("empty-statement")
    private void repairAnime() throws SQLException, AnidbException {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT aid FROM files WHERE aid NOT IN (SELECT "
                    + "anime.aid FROM files, anime WHERE files.aid = anime.aid);");
            while (rs.next())
                insertAnime(rs.getInt("aid"));
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
            try { if (stmt != null) stmt.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Adds all missing episodes informations
     *
     * @return true if success
     */
    @SuppressWarnings("empty-statement")
    private boolean repairEpisodes() throws SQLException, AnidbException {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT eid FROM files WHERE eid NOT IN (SELECT "
                    + "episodes.eid FROM files, episodes WHERE files.eid = episodes.eid);");
            while (rs.next()) {
                insertEpisode(rs.getInt("eid"));
            }
            return true;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
            try { if (stmt != null) stmt.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Adds all missing group informations
     *
     * @return true if success
     */
    @SuppressWarnings("empty-statement")
    private boolean repairGroup() throws AnidbException, SQLException {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT gid FROM files WHERE gid NOT IN (SELECT "
                    + "groups.gid FROM files, groups WHERE files.gid = groups.gid);");
            while (rs.next()) {
                insertGroup(rs.getInt("gid"));
            }
            return true;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
            try { if (stmt != null) stmt.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Removes entry from FilesToAdd by given hash value.
     *
     * @param hash ed2k hash of file we want to delete
     * @throws java.sql.SQLException
     */
    public void deleteFromFilesToAdd(String hash) throws SQLException {
        try (PreparedStatement prepStmt = conn.prepareStatement("DELETE FROM files_to_add WHERE ed2k = ?;")) {
            prepStmt.setString(1, hash);
            prepStmt.execute();
        }
    }

    /**
     * Removes entry from FilesToAdd by given path.
     *
     * @param path to a file we want to delete from files_to_add db.
     * @throws java.sql.SQLException
     */
    public void deleteFromFilesToAdd(Path path) throws SQLException {
        try (PreparedStatement prepStmt = conn.prepareStatement("DELETE FROM files_to_add WHERE path = ?;")) {
            prepStmt.setString(1, path.toString());
            prepStmt.execute();
        }
    }

    /**
     * Returns fid of file with given path
     *
     * @param path - path to a file we want fid of
     * @return fid or -1 when error
     * @throws java.sql.SQLException
     */
    @SuppressWarnings("empty-statement")
    public int getFilesToAddFid(Path path) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT fid FROM files_to_add WHERE path = ? ;")) {
            prepStmt.setString(1, path.toString());
            rs = prepStmt.executeQuery();
            return rs.getInt("fid");
        } catch (NullPointerException e) {
            throw new SQLException("No such file in local db.");
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {};
        }
    }

    /**
     * Update file id value in files to add.
     *
     * @param path path to a file we want to upgrade
     * @param fid new file id of file.
     * @throws java.sql.SQLException
     */
    public void updateFilesToAdd(Path path, int fid) throws SQLException {
        try (PreparedStatement prepStmt = conn.prepareStatement("UPDATE files_to_add SET fid = ? WHERE path = ?;")) {
            prepStmt.setInt(1, fid);
            prepStmt.setString(2, path.toString());
            prepStmt.execute();
        }
    }

    /**
     * Update hash value in files to add.
     *
     * @param hash - new hash for file
     * @param path - path to a file
     * @throws java.sql.SQLException
     */
    public void updateFilesToAdd(Path path, String hash) throws SQLException {
        try (PreparedStatement prepStmt = conn.prepareStatement("UPDATE files_to_add SET ed2k = ? WHERE path = ?;")) {
            prepStmt.setString(1, hash);
            prepStmt.setString(2, path.toString());
            prepStmt.execute();
        }
    }

    public void deleteEmptyFromFilesToAdd() throws SQLException {
        try (Statement stat = conn.createStatement()) {
            stat.execute("DELETE FROM files_to_add WHERE ed2k='0';");
        }
    }

    /**
     * Insert anime description into db. But first we need to get it from anidb.
     * <br />This function calls {@link #formatAnimeDescription(java.lang.String)}
     * before inserting description.
     * @param aid id of anime we want to download description
     * @return returns anime description after putting it into local db.
     * @throws AnidbException when we can't get description from anidb
     * @throws SQLException when we can't put description into local db.
     */
    private String insertAnimeDescription(int aid) throws AnidbException, 
            SQLException {
        String description;
        try {
            description = ah.importAnimeDescription(aid);
        } catch (AnidbMissingContentException e) {
            description = "No Description in Anidb.";
        }
        try(PreparedStatement prepStmt = conn.prepareStatement("INSERT INTO "
                + "description VALUES (?, ?);")) {
            prepStmt.setInt(1, aid);
            description = this.formatAnimeDescription(description);
            prepStmt.setString(2, description);
            prepStmt.execute();
            return description;
        }
    }

    /**
     * Change bbcode found in anidb description to URL - anidb uses only i, url 
     * and b codes, its not optimized to change other codes!
     * @param desc description in which we want to change bbcode to html
     * @return string containing html not bbcode
     * 
     * TODO: optimize URL change.
     */
    private String formatAnimeDescription(String desc) {
        desc = desc.replaceAll("\\[","<");
        desc = desc.replaceAll("\\]",">");
        if(desc.matches(".*<url.*")) {
            String[]tmp = desc.split("<url");
            for(int i = 0; i < tmp.length; i++) {
                if(tmp[i].startsWith("=")) {
                    tmp[i] = tmp[i].replaceFirst("=", "<a href=\"");
                    tmp[i] = tmp[i].replaceFirst(">", "\">");
                    tmp[i] = tmp[i].replaceFirst("</url>", "</a>");
                }
            }
            StringBuilder sb = new StringBuilder();
            for(String temp: tmp)
                sb.append(temp);
            desc = sb.toString();
        }
        return desc;
    }

    /**
     * Deletes all data from other_files table. 
     * @throws java.sql.SQLException
     */
    public void deleteFromOtherFiles() throws SQLException {
        try (PreparedStatement prepStmt = conn.prepareStatement("DELETE FROM other_files;")) {
            prepStmt.execute();
        }
    }
    
    /**
     * Creates Anime Map with full info from db. Generally converts result set 
     * from db to a {@link anidbjclient.lib.Anime} instance.
     * @return map with local anime cache, or null when no anime info cached
     * @throws SQLException when we can't get data from database.
     */
    public Map<Integer, Anime> getAnimeMap() throws SQLException {
        Map<Integer, Anime> map;
        /* Check how many anime we got in db and create map with spec size */
        try (Statement stat = conn.createStatement(); ResultSet rs = 
                stat.executeQuery("SELECT COUNT(*) AS total FROM anime;")) {
            rs.next();
            int total = rs.getInt("total");
            if (total == 0)
                return null;
            map = new HashMap<>(total);
        }
        try (Statement stat = conn.createStatement(); ResultSet rs = 
                stat.executeQuery("SELECT *, CASE WHEN anime.aid IN (SELECT "
                        + "anime.aid FROM anime, files, my_list WHERE anime.aid "
                        + "= files.aid AND files.fid = my_list.fid GROUP BY anime.aid "
                        + "HAVING viewed_date = -1) THEN 0 ELSE 1 END AS "
                        + "watched, (SELECT COUNT(eid) FROM episodes WHERE "
                        + "episodes.type = 1 AND episodes.aid = anime.aid) AS "
                        + "owned_episodes FROM anime;")) {
            while(rs.next()) {
                map.put(rs.getInt("aid"), new Anime(rs));
            }
        }
        return map;
    }
    
    public Anime getAnime(int aid) throws SQLException {
        try (Statement stat = conn.createStatement(); ResultSet rs = stat.executeQuery("SELECT *, CASE WHEN anime.aid IN (SELECT "
                        + "anime.aid FROM anime, files, my_list WHERE anime.aid "
                        + "= files.aid AND files.fid = my_list.fid GROUP BY anime.aid "
                        + "HAVING viewed_date = -1) THEN 0 ELSE 1 END AS "
                        + "watched, (SELECT COUNT(eid) FROM episodes WHERE "
                        + "episodes.type = 1 AND episodes.aid = anime.aid) AS "
                        + "owned_episodes FROM anime WHERE aid = " + aid)) {
            return new Anime(rs);
        }
    }
    
    public Map<Integer, Episode> getEpisodesMap(int aid) throws SQLException {
        Map<Integer, Episode> map;
        ResultSet rs;
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT COUNT(*) "
                + "AS total FROM files WHERE aid = ? ;")) {
            prepStmt.setInt(1, aid);
            rs = prepStmt.executeQuery();
            int total = rs.getInt("total");
            if (total == 0)
                return null;
            map = new HashMap<>(total);
        }
        try (PreparedStatement prepStmt = conn.prepareStatement("SELECT * FROM "
                + "files WHERE aid = ? ;")) {
            prepStmt.setInt(1, aid);
            rs = prepStmt.executeQuery();
            while(rs.next())
                map.put(rs.getInt("fid"), new Episode(rs));
        }
        return map;
    }
    
    public Map<String, List<Object>> testResult() throws SQLException {
        try (Statement stat = conn.createStatement(); ResultSet rs = 
                stat.executeQuery("SELECT * FROM anime;")) {
            return Convert.resultSetToHashMap(rs);
        }
    }
}
