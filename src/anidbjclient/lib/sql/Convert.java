/*
 * Copyright (C) 2014 Krzysztof Troska (elleander86 at gmail.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib.sql;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public class Convert {

    public static Map<String, List<Object>> resultSetToHashMap(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        Map<String, List<Object>> map = new HashMap<>();
        int columns = rsmd.getColumnCount();
        rsmd.getColumnType(0);
        if (columns <= 0) {
            return null;
        }
        for (int i = 1; i < columns; i++) {
            map.put(rsmd.getColumnLabel(i), new ArrayList<>());
        }
        while (rs.next()) {
            for (int i = 1; i < columns; i++) {
                map.get(rsmd.getColumnLabel(i)).add(rs.getObject(i));
            }
        }
        return map;
    }
}
