/*
 * Copyright (C) 2014 Krzysztof Troska (elleander86 at gmail.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib;

import anidbjclient.lib.anidb.ConfigAnidb;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public final class ConfigManagerAnidbJClient implements ConfigAnidb {

    private final ConfigManager cm;

    public ConfigManagerAnidbJClient(ConfigManager cm) {
        this.cm = cm;
    }

    /**
     * Sets local port we just check if port is higher than 1024 and lower than
     * 65535.
     *
     * @param port
     * @return
     */
    public boolean setLocalPort(int port) {
        if (port < 1024 || port > 65535) {
            System.err.println("Local port need to be number bettwen 1025 and "
                    + "65535, no change made");
            return false;
        } else {
            cm.setOption("LocalPort", Integer.toString(port));
            return true;
        }
    }

    /**
     * Get local port number from config file if local port is not between
     * 1025-65535 we return default
     *
     * @return local port or default 22356
     */
    @Override
    public int getLocalPort() {
        try {
            int port = Integer.valueOf((String) cm.getOption("LocalPort"));
            if (port > 1024 && port < 65535) {
                return port;
            }
        } catch (ClassCastException | NullPointerException | NumberFormatException ignore) {
            System.err.println("Wrong port number using default.");
        }
        return 22356;
    }

    /**
     * Returns anidb API port.
     *
     * @return
     */
    @Override
    public int getAnidbPort() {
        try {
            int port = Integer.parseInt(cm.getOption("Port"));
            if (port > 0 && port < 65535) {
                return port;
            }
        } catch (NumberFormatException e) {
            System.err.println("Wrong port number using default.");
        }
        return 9000;
    }

    /**
     * Gets user name or asks for it if not exists
     *
     * @return
     */
    @Override
    public String getUserName() {
        return cm.getOption("User");
    }

    /**
     * Gets password or asks for it if not exists
     *
     * @return
     */
    @Override
    public String getUserPass() {
        return cm.getOption("Password");
    }

    /**
     * Get inet Address of anidb server
     *
     * @return inet address of anidb or null when DNS error
     */
    @Override
    public InetAddress getServerAddress() {
        try {
            return InetAddress.getByName(cm.getOption("Server"));
        } catch (UnknownHostException e) {
            System.err.println("Cant resolve anidb server adres trying to use default one.");
            try {
                return InetAddress.getByName("api.anidb.net");
            } catch (UnknownHostException e1) {
                System.err.println("Cant resolve default anidb server name check internet connection.");
                return null;
            }
        }
    }

    @Override
    public String getUserApiPass() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getClientVersion() {
        return 1;
    }

    @Override
    public String getClientName() {
        return "anidbjclient";
    }

    @Override
    public String getFileFMask() {
        return "78000008D0";
    }

    @Override
    public String getFileAMask() {
        return "00000080";
    }

    @Override
    public String getAnimeAMask() {
        return "F0809A01000000";
    }

    @Override
    public int getBuffSize() {
        return 1400;
    }

    /**
     * Checks if given extension is in our list
     *
     * @param extension
     * @return true if list contains extension
     */
    public boolean compereExtensions(String extension) {
        List<String> extensions = Arrays.asList(cm.getOption("Extensions").split("\\|"));
        return extensions.contains(extension);
    }

    /**
     * Returns download Path.
     *
     * @return
     */
    public Path getDownloadPath() {
        try {
            return Paths.get(cm.getOption("Download_Folder"));
        } catch (InvalidPathException e) {
            System.err.println("Invalid Download path using default.");
        }
        return Paths.get(System.getProperty("user.home"), "Downloads");
    }

    /**
     * Returns path to a directory containing pictures of anime from anidb.
     *
     * @return
     * @throws java.io.IOException
     */
    public Path getPicPath() throws IOException {
        Path path;
        try {
            path = Paths.get(cm.getOption("Pictures_Folder"));
        } catch (InvalidPathException e) {
            System.err.println("Invalid Pictures path using default.");
            path = Paths.get(new File(".").getAbsolutePath(), "pics");
        }
        if (!path.toFile().exists() && !path.toFile().mkdirs()) {
            throw new IOException("Can't create pics directory in getAnimePic()");
        }
        return path;
    }

    /**
     * Sets download directory we just check if it's real directory
     *
     * @param path path to directory
     * @return ture if success
     */
    public boolean setDownloadPath(Path path) {
        if (path.toFile().isDirectory()) {
            cm.setOption("Download_Folder", path.toString());
            return true;
        } else {
            System.err.println("Path is not a directory.");
            return false;
        }
    }

    /**
     * Returns path to anime directory.
     *
     * @return
     */
    public Path getAnimePath() {
        try {
            return Paths.get(cm.getOption("Anime_Folder"));
        } catch (InvalidPathException e) {
            System.err.println("Invalid Anime path using default.");
        }
        return Paths.get(System.getProperty("user.home"), "Videos", "Anime");
    }

    /**
     * Sets anime path, we just check if it's directory.
     *
     * @param path - path to a directory
     * @return ture if success
     */
    public boolean setAnimePath(Path path) {
        if (path.toFile().isDirectory()) {
            cm.setOption("Anime_Folder", path.toString());
            return true;
        } else {
            System.err.println("Path is not a directory.");
            return false;
        }
    }

    /**
     * Sets anidb port, won't change it if the port is higher than 65535 or
     * lower than 1.
     *
     * @param port
     * @return ture if success
     */
    public boolean setPort(int port) {
        if (port < 1 || port > 65535) {
            System.err.println("Anidb port need to be number bettwen 1 and "
                    + "65535, no change made");
            return false;
        } else {
            cm.setOption("Port", Integer.toString(port));
            return true;
        }
    }

    /**
     * Sets stored user name.
     *
     * @param name string of a name
     */
    public void setUserName(String name) {
        if (!name.isEmpty()) {
            cm.setOption("User", name);
        } else {
            System.err.println("User name can't be empty.");
        }
    }

    /**
     * Sets the password value for security reasons array is then filled with
     * zeros (but still the password is stored inside txt file so there is no
     * security).
     *
     * @param password password in char array
     */
    public void setPassword(char[] password) {
        if (password.length > 0) {
            cm.setOption("Password", new String(password));
            Arrays.fill(password, '0');
        } else {
            System.err.println("Password can't be empty.");
        }
    }

    /**
     * Get server name as a string for future change.
     *
     * @return - Server name
     */
    public String getServerName() {
        return cm.getOption("Server");
    }

    /**
     * Reads the option Auto_Scan, it true ONLY when config file line looks like
     * Auto_Scan=true in all other cases its false. By default this option is
     * set as false since we want to
     *
     * @return
     */
    public boolean autoScan() {
        return Boolean.getBoolean(cm.getOption("Auto_Scan"));
    }

    /**
     * Converts given boolean value to string and saves it.
     *
     * @param scan - true if we want to scan download directory
     */
    public void setAutoScan(boolean scan) {
        cm.setOption("Auto_Scan", Boolean.toString(scan));
    }
}
