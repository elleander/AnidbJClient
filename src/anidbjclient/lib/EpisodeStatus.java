/*
 * Copyright (C) 2014 Krzysztof Troska (elleander86 at gmail.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib;

/**
 *
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public enum EpisodeStatus {

    /**
     * Status is unknown, file needs to be checked against anidb.
     */
    UNKNOWN,
    /**
     * File is not in anidb - its not anime.
     */
    OTHER,
    /**
     * When we know ed2k hash.
     */
    HASHED,
    /**
     * When we did counted ed2k and asked a anidb, but anidb returned.
     * {@link anidbjclient.lib.anidb.AnidbMissingContentException}
     */
    NOT_ANIDB,
    /**
     * When we got file inside db but its still not moved.
     */
    IN_DB,
    /**
     * File is inside local library all is good.
     */
    STORED,
    /**
     * File is stored inside library but its missing on HDD. File was deleted by
     * 3rd application.
     */
    DELETED;
}
