/*
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib.anidb;

/**
 * Exception returned when we send good comand to anidb but anidb dosn't have
 * any info about e.g. anime or file. Should be handled by giving user a info
 * that this file/anime/group etc do not exist in anidb.
 *
 * @version 0.1.0, 09/19/14
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public class AnidbMissingContentException extends AnidbException {

    private static final long serialVersionUID = 1L;

    /**
     * Basic exception without any additional info.
     */
    public AnidbMissingContentException() {
        super();
    }

    /**
     * Exception with aditional usualy its anidb server response.
     * @param msg info from anidb server.
     */
    public AnidbMissingContentException(String msg) {
        super(msg);
    }
}
