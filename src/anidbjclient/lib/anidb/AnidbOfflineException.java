/*
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib.anidb;

/**
 * When we detected that anidb is offline (not responding), all constructors
 * sets offline mode on, by call in
 * {@link anidbjclient.lib.anidb.AnidbException}.
 *
 * @version 0.1.0, 09/19/14
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public class AnidbOfflineException extends AnidbException {

    private static final long serialVersionUID = 1L;

    /**
     * Bassic offline exception anidb, anidb is not responding.
     */
    public AnidbOfflineException() {
        super();
        AnidbException.goOffline();
    }

    /**
     * Offline exception with a reason (usualy no internet connection).
     *
     * @param msg
     */
    public AnidbOfflineException(String msg) {
        super(msg);
        AnidbException.goOffline();
    }
}
