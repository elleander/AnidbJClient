/* 
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib.anidb;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Client side anidb support API with basic options. There is still some work to
 * do but it generally works and is suitable to use in other projects.
 *
 * @version 0.1.0, 09/19/14
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public class AnidbHandler {

    private final ConfigAnidb cm;

    /**
     * Port as seen by anidb, if its set to -1 we are not loged or anidb did not
     * give us port number. Try to use {@link #ping()} to get a updated port
     * number.
     */
    public static int NAT_PORT = -1;

    /**
     * True if we are logged, you can use it to check if client needs to logout.
     */
    public static boolean isLoged = false;

    /**
     * Stores current sessionID.
     */
    private static String sessionID;

    /**
     * We are working in offline mode anidb haven't responded in time.
     */
    public static boolean offlineMode = false;

    /**
     * Time in millis of when the {@link #offlineMode} flag was set.
     */
    public static long offlineTime = 0;

    /**
     * Current Time in Millis of last send packet, You can use it to check when
     * was the last time we send packet to anidb.net. On init this function is
     * set to current time - 4000 ms.
     */
    public static long sendTime = System.currentTimeMillis() - 4000;

    /**
     * Only loads config into memory, config settings are final and to load
     * other settings you need to create new function.
     *
     * @param cm {@link anidbjclient.lib.anidb.ConfigAnidb} used to store
     * settings.
     */
    public AnidbHandler(ConfigAnidb cm) {
        this.cm = cm;
    }

    /**
     * Changing our message to be anidb friendly. Appends /n and changes session
     * id to current one if needed.
     *
     * @param msg message we want to send.
     * @return
     * @throws AnidbException if we are not logged or can't log (wrong password)
     * anidb problems
     */
    private String readyMessage(String msg) throws AnidbException {
        /* commands for which we don't need to log */
        msg = msg.trim();
        List<String> noLoggin = Arrays.asList("PING", "ENCRYPT", "ENCODING",
                "AUTH", "VERSION");
        String command;
        if (msg.contains(" ")) {
            command = msg.substring(0, msg.indexOf(" "));
        } else {
            command = msg; //e.g. LOGOUT
        }
        if (noLoggin.contains(command)) {
            if (msg.contains("\n")) {
                return msg; // we already got \n at end so dont bother
            } else {
                return msg + "\n"; // command don't need login so we just add eol
            }
        }
        /* log if not logged */
        if (!isLoged) {
            connect();
        }
        /* if our option only needs sid (e.g. logout) */
        if (msg.indexOf(' ') == -1) {
            msg += " s=" + sessionID + "\n";
            return msg;
        }
        /* cut out old session id if we already got one if not just add it */
        if (msg.contains("&s=")) {
            msg = msg.substring(0, msg.indexOf("&s="));
            msg = msg.concat("&s=" + sessionID);
        } else if (msg.contains("s=")) {
            msg = msg.substring(0, msg.indexOf("s="));
            msg = msg.concat(" s=" + sessionID);
        } else {
            msg = msg.concat("&s=" + sessionID);
        }
        return msg + "\n";
    }

    /**
     * This function sets all flags that needs to be set when going offline
     * mode. It don't send any data to anidb.
     */
    public static void goOffline() {
        offlineMode = true;
        offlineTime = System.currentTimeMillis();
        isLoged = false;
        NAT_PORT = -1;
    }

    /**
     * Process message and sends it to anidb also checks response and returns
     * it. This function is synchronized - since we will get banned if we use
     * too many ports, thats why we only want one port open and only one message
     * per 4 sec.
     *
     * @param msg raw message to be send to anidb.
     * @param acceptCodes all codes we expect, all other codes will throw
     * exception if no code given we pass all responses as good and return them
     * @return response from anidb
     * @throws AnidbOfflineException() when we are in offline mode.
     * @throws AnidbException() when we get a fatal error (like no login info
     * given)
     */
    private synchronized String sendData(String msg, int... acceptCodes) throws
            AnidbOfflineException, AnidbException {
        /* if we are in offline mode send only ping to test if API is online */
        if (offlineMode && !msg.startsWith("PING")) {
            throw new AnidbOfflineException();
        }
        DatagramPacket packet;
        /**
         * There is *generally* no way for our message be longer than 1400 bytes
         * TODO: but we still want to take care of that at some time.
         */
        byte[] buf;
        String response;
        int code;
        long wait;
        int tries = 0;
        while (true) {
            /* make msg anidb friendly */
            msg = readyMessage(msg);
            if (tries++ >= 3) {
                throw new AnidbOfflineException("Can't get response after 3 tries.");
            }
            try (DatagramSocket socket = new DatagramSocket(cm.getLocalPort())) {
                wait = System.currentTimeMillis() - sendTime;
                /**
                 * Wait at least 4 s from last send
                 */
                if (wait < 4000) {
                    try {
                        TimeUnit.MILLISECONDS.sleep(4000 - wait);
                    } catch (InterruptedException ignore) {
                        System.err.println("Wait was interupted.");
                    }
                }
                buf = msg.getBytes(); /* send msg to buffer */
                /* remove password before printing it in log */
                if (msg.contains("&pass")) {
                    System.out.println("Sending msg: " + msg.replaceFirst(
                            "&pass[^&]*", "&pass=***"));
                } else {
                    System.out.println("Sending msg: " + msg);
                }
                packet = new DatagramPacket(buf, buf.length, cm.getServerAddress(),
                        cm.getAnidbPort());
                socket.send(packet);
                sendTime = System.currentTimeMillis();
                buf = new byte[cm.getBuffSize()]; // clear buffer
                packet = new DatagramPacket(buf, buf.length);
                socket.setSoTimeout(10000); // 10 seconds wait before trying again
                socket.receive(packet);
                response = new String(buf).trim();
                if (!response.contains(" ")) // if no " " then we can't extract code.
                {
                    throw new AnidbException("Unexpected response: " + response);
                }
                code = this.getResponseCode(response, msg);
                System.out.format("Received code: %d, Reponse; %s\n", code, response);
                /* If we get log time out, we will login again. */
                if (code == 501 || code == 502 || code == 506) {
                    isLoged = false;
                    continue;
                }
                /* no accepted codes so we take all responses as good */
                if (acceptCodes.length == 0) {
                    return response;
                }
                /* if at least 1 accepted code is ok then we return response */
                for (int acceptCode : acceptCodes) {
                    if (acceptCode == code) {
                        return response;
                    }
                }
                /*
                 * Response did not pass code check so its a problem. We handle
                 * it behind try-catch block.
                 */
            } catch (SocketTimeoutException e) {
                /**
                 * Socket did time out so we start whole thing again.
                 */
                System.out.println("Socket Time out, trying again.");
                continue;
            } catch (SocketException e) {
                throw new AnidbException("Can't create socket");
            } catch (IOException ex) {
                throw new AnidbException("Can't read from socket");
            }
            if (!response.isEmpty()) {
                checkResponseCode(response, msg);
            } else {
                throw new AnidbException("Wrong response");
            }
        }
    }

    /**
     * Sends AUTH command to anidb, usable to test if given account exists
     * before saving it in config. If you need to test if anidb api is online
     * than its better to use {@link #ping()} since you don't need to worry with
     * logging out.
     *
     * @param name - user name
     * @param password - user password
     * @throws anidbjclient.lib.anidb.AnidbWrongLoginException given name/pass
     * is wrong.
     * @throws anidbjclient.lib.anidb.AnidbOfflineException Anidb is prodobly
     * offline (not responding).
     * @throws AnidbException fatal anidb error, or wrong input (e.g. not usable
     * signs in user name/pass)
     */
    public void connect(String name, String password) throws AnidbWrongLoginException,
            AnidbOfflineException, AnidbException {
        StringBuilder sb = new StringBuilder();
        sb.append("AUTH ");
        sb.append("user=");
        sb.append(name);
        sb.append("&pass=");
        sb.append(password);
        sb.append("&protover=");
        sb.append(3);
        sb.append("&client=");
        sb.append(cm.getClientName());
        sb.append("&clientver=");
        sb.append(cm.getClientVersion());
        sb.append("&nat=1");
        String response = sendData(sb.toString(), 200, 201, 500);
        int code = this.getResponseCode(response);
        String[] responseArray = response.split(" ");
        if (code == 200 || code == 201) {
            sessionID = responseArray[1];
            /* check for nat at work */
            if (response.contains(":")) {
                /* extract port number from response */
                try {
                    int port = Integer.parseInt(response.substring(response.indexOf(":") + 1,
                            response.indexOf(" ", response.indexOf(":"))));
                    NAT_PORT = port;
                } catch (Exception ignore) {
                    NAT_PORT = -1; // problem with getting nat port
                }
            }
            System.out.format("Session set: %s, port: %b\n", sessionID, NAT_PORT);
            isLoged = true;
        }
        if (code == 201) {
            System.err.println("New Version avaible, consider upgrading.");
        } else if (code == 500) {
            throw new AnidbWrongLoginException();
        }
    }

    /**
     * Connect using name and password from config file. Use it if you know
     * saved login info is good.
     *
     * @throws anidbjclient.lib.anidb.AnidbWrongLoginException Wrong name/pass
     * stored in config.
     * @throws anidbjclient.lib.anidb.AnidbOfflineException Anidb api is not
     * responding.
     * @throws AnidbException fatal anidb error, or wrong input (e.g. not usable
     * signs in user name/pass)
     */
    public void connect() throws AnidbWrongLoginException,
            AnidbOfflineException, AnidbException {
        connect(cm.getUserName(), cm.getUserPass());
    }

    /**
     * Gets response code from anidb response, if there is problem with that it
     * throws AnidbException with command and response so we know what went
     * wrong.
     *
     * @param response response from anidb.
     * @param command command we send to anidb.
     * @return int containing XXX anidb response code.
     * @throws AnidbException in case response code can't be read.
     */
    private int getResponseCode(String response, String command) throws AnidbException {
        int code;
        try {
            code = Integer.parseInt(response.substring(0, 3));
        } catch (NumberFormatException e) {
            // should not happend but still.
            if (command.isEmpty()) {
                throw new AnidbException(response);
            } else {
                throw new AnidbException(response + "\n" + command);
            }
        }
        return code;
    }

    /**
     * Gets response code from anidb response, if there is problem with that it
     * throws AnidbException with response we got so we know what went wrong.
     *
     * @param response - response from anidb
     * @return - int containing XXX anidb response code.
     * @throws AnidbException in case response code can't be read
     */
    private int getResponseCode(String response) throws AnidbException {
        return getResponseCode(response, "");
    }

    /**
     * Checks response code and handles that with throwing corresponding anidb
     * exception. This function <b>always</b> throws exception.
     *
     * @param response full response from anidb server
     * @param command full command we sent for anidb server for reference
     * @throws AnidbException its probably internal anidb error, check message.
     * @throws AnidbOfflineException its probably our fault pleas check response.
     */
    private void checkResponseCode(String response, String command) throws
            AnidbOfflineException, AnidbException {
        int code = this.getResponseCode(response, command);
        /* get all after error code and drop new line mark */
        String msg = response.substring(4).replaceAll("\\n", "");
        if (code >= 700) {
            throw new AnidbException("Unknow error code:\n" + msg);
        } else if (code >= 600) { //internal anidb error
            //TODO: should make some wait option for next questions 
            throw new AnidbOfflineException("Internal Anidb error:\n" + msg);
        }
        switch (code) {
            case 500:
                throw new AnidbOfflineException("Wrong login or/and password"
                        + " in config file.");
            // Banned:
            case 555:
                throw new AnidbOfflineException("We got banned reson: " + msg);
            //other prodobly our foult
            case 598:
            case 505:
            default:
                throw new AnidbException(msg + "\n" + command);
        }
    }

    /**
     * We just want to import data from AniDB to local cache. This function
     * imports data from anidb accordingly to:
     * <a href="http://wiki.anidb.net/w/UDP_API_Definition#FILE:_Retrieve_File_Data">this
     * guide</a> and returns all info in String array in order given by anidb.
     * This function also adds given hash to the last place in array.
     *
     * @param path to the file we want to import information
     * @param hash String containing ed2k hash, <u>appended to the end of
     * returned array</u>
     * @return String array for later add into local cache. Example return:
     * fileFMasktmp2: 0-fid 1-aid 2-eid 3-gid 4-myListId 5-air date 6- mylist
     * state 7-mylistfilestate 8- viewdate than from fileAMask: 9- group name
     * Other: <b>10- hash</b>. Always as the last place is ed2k hash.
     * @throws anidbjclient.lib.anidb.AnidbMissingContentException no such file
     * in anidb
     * @throws AnidbException in case we did not import data from anidb
     */
    public String[] importFileInfo(Path path, String hash)
            throws AnidbMissingContentException, AnidbException {
        File file = path.toFile();
        StringBuilder sb = new StringBuilder();
        sb.append("FILE ");
        sb.append("size=");
        sb.append(file.length());
        sb.append("&ed2k=");
        sb.append(hash);
        sb.append("&fmask=");
        sb.append(cm.getFileFMask());
        sb.append("&amask=");
        sb.append(cm.getFileAMask());
        String response = sendData(sb.toString(), 220, 320);
        int code = this.getResponseCode(response);
        if (code == 220) {
            String[] tmp = response.split("\n");
            String[] tmp2 = tmp[1].split("\\|");
            return addStringArrays(tmp2, hash);
        } else if (code == 320) {
            throw new AnidbMissingContentException(response);
        }
        throw new AnidbException(response);
    }

    /**
     * Imports episode info. Look:
     * <a href="http://wiki.anidb.net/w/UDP_API_Definition#EPISODE:_Retrieve_Episode_Data">EPISODE</a>.
     *
     *
     * @param eid id of a episode in a anidb.
     * @return array of strings (can have some empty values!) 0- eid 1-aid
     * 2-length 3- rating 4- votes 5-ep_number 6-eng_name 7- romaji_name 8-kanji
     * 9-aired 10-type
     * @throws anidbjclient.lib.anidb.AnidbMissingContentException there is no
     * episode with that episode id.
     * @throws AnidbException when anidb returned error
     */
    public String[] importEpisodeInfo(int eid) throws
            AnidbMissingContentException, AnidbException {
        StringBuilder sb = new StringBuilder();
        sb.append("EPISODE ");
        sb.append("eid=");
        sb.append(eid);
        String response = sendData(sb.toString(), 240, 340);
        int code = this.getResponseCode(response);
        if (code == 240) {
            String[] tmp = response.split("\n");
            String[] tmp2 = tmp[1].split("\\|");
            return tmp2;
        } else if (code == 340) {
            throw new AnidbMissingContentException(response);
        }
        throw new AnidbException(response);
    }

    /**
     * Imports group info. Look
     * <a href="http://wiki.anidb.net/w/UDP_API_Definition#GROUP:_Retrieve_Group_Data">GROUP</a>.
     *
     * @param gid
     * @return array of strings (can have some empty values) 0-gid 1-rating
     * 2-votes 3-acount 4-fcount 5-name 6-short 7-ircChannel 8-ircServer 9-url
     * 10-picname 11-founddate 12-disbandeddate 13-dateflags 14-lastrelesedate
     * 15-lastactivitydate 16-groupdrelations.
     * @throws anidbjclient.lib.anidb.AnidbMissingContentException there is no
     * such group.
     * @throws AnidbException when we got anidb error.
     */
    public String[] importGroupInfo(int gid) throws
            AnidbMissingContentException, AnidbException {
        StringBuilder sb = new StringBuilder();
        sb.append("GROUP ");
        sb.append("gid=");
        sb.append(gid);
        String response = sendData(sb.toString(), 250, 350);
        int code = this.getResponseCode(response);
        if (code == 250) {
            String[] tmp = response.split("\n");
            String[] tmp2 = tmp[1].split("\\|");

            return tmp2;
        } else if (code == 350) {
            throw new AnidbMissingContentException(response);
        }
        throw new AnidbException(response);
    }

    /**
     * Imports anime description from anidb. It returns anidb description as is,
     * so using bbcodes for format.
     *
     * @param aid - anime id
     * @return - anime description
     * @throws anidbjclient.lib.anidb.AnidbMissingContentException no such
     * anime, or no such description in anidb.
     * @throws AnidbException - when we got anidb error
     */
    public String importAnimeDescription(int aid) throws
            AnidbMissingContentException, AnidbException {
        int part = 0, parts, code;
        String query, response;
        String[] tmp, tmp2;
        StringBuilder description = new StringBuilder();
        do {
            query = animeDescriptionQuery(aid, part);
            response = sendData(query, 233, 333);
            code = this.getResponseCode(response);
            tmp = response.split("\n");
            if (code == 233) {
                tmp2 = tmp[1].split("\\|");
                parts = Integer.parseInt(tmp2[1]);
                /* walk around Anidb BUG:0002072 */
                if (tmp2.length == 3 && !tmp2[2].isEmpty()) {
                    description.append(tmp2[2]);
                }
            } else {
                throw new AnidbMissingContentException(response);
            }
            part++;
        } while (part <= parts);
        return description.toString().replaceAll("<cut>", "");
    }

    /**
     * Internal query make function, makes query for a given part.
     *
     * @param aid int anime id
     * @param part int part of description
     * @return query to be send
     */
    private String animeDescriptionQuery(int aid, int part) {
        StringBuilder sb = new StringBuilder();
        sb.append("ANIMEDESC ");
        sb.append("aid=");
        sb.append(aid);
        sb.append("&part=");
        sb.append(part);
        return sb.toString();
    }

    /**
     * Returns my list info for given my list id.
     *
     * @param myListId my list id
     * @return array of strings (some places can be empty) 0 - lid, 1 - fid, 2 -
     * eid, 3- aid, 4-gid, 5-date, 6- state, 7-viewdate, 8-storage, 9-source,
     * 10-other, 11-filestate
     * @throws anidbjclient.lib.anidb.AnidbMissingContentException no such my
     * list id.
     * @throws AnidbException anidb error.
     */
    public String[] importMyListInfo(int myListId) throws
            AnidbMissingContentException, AnidbException {
        StringBuilder sb = new StringBuilder();
        sb.append("MYLIST ");
        sb.append("lid=");
        sb.append(myListId);
        String response = sendData(sb.toString(), 221, 321);
        int code = this.getResponseCode(response);
        if (code == 221) {
            String[] tmp = response.split("\n");
            String[] tmp2 = tmp[1].split("\\|");
            return tmp2;
        } else {
            throw new AnidbMissingContentException(response);
        }
    }

    /**
     * Gets additional info from anidb accessible only by ANIME command. Look
     * <a href="http://wiki.anidb.net/w/UDP_API_Definition#ANIME:_Retrieve_Anime_Data">ANIME</a>
     * for more info.
     *
     * @param aid anime id to get additional info
     * @return array depends on given animeAMask e.g. 0 aid/ 1 dateflags/ 2 year
     * /3 type/ 4 name/5 episodes/6 air date/ 7end date/ 8 pic name/ 9/ 18+
     * rest.
     * @throws anidbjclient.lib.anidb.AnidbMissingContentException no such anime
     * @throws AnidbException when anidb returned error
     */
    public String[] importAnimeInfo(int aid) throws
            AnidbMissingContentException, AnidbException {
        StringBuilder sb = new StringBuilder();
        sb.append("ANIME ");
        sb.append("aid=");
        sb.append(aid);
        sb.append("&amask=");
        sb.append(cm.getAnimeAMask());
        String response = sendData(sb.toString(), 230, 330);
        int code = this.getResponseCode(response);
        if (code == 230) {
            String[] tmp = response.split("\n");
            String[] tmp2 = tmp[1].split("\\|");
            return tmp2;
        } else {
            throw new AnidbMissingContentException(response);
        }
    }

    private String[] addStringArrays(String[] a, String b) {
        String[] result = new String[a.length + 1];
        System.arraycopy(a, 0, result, 0, a.length);
        result[a.length] = b;
        return result;
    }

    /**
     * Adds file to my list. Uses default state which is 1 (on HDD).
     *
     * @param fid file id
     * @param viewDate time of view in Unix time stamp
     * @return new my list id, or old one if already existed.
     * @throws AnidbException when anidb returned error
     */
    public int addToMyList(int fid, long viewDate) throws AnidbException {
        StringBuilder sb = new StringBuilder();
        sb.append("MYLISTADD ");
        sb.append("fid=");
        sb.append(fid);
        sb.append("&state=");
        sb.append("1");
        sb.append("&viewed=");
        if (viewDate > 0) {
            sb.append("1");
            sb.append("&viewdate=");
            sb.append(viewDate);
        } else {
            sb.append("0");
        }
        String response = sendData(sb.toString(), 210, 310);
        int code = this.getResponseCode(response);
        String[] tmp = response.split("\n");
        if (code == 210) {
            return Integer.parseInt(tmp[1]);
        } else {
            String[] tmp2 = tmp[1].split("\\|");
            return Integer.parseInt(tmp2[0]);
        }
    }

    /**
     * Updates my lsit entry
     *
     * @param lid myListId
     * @param state int state of file see
     * http://wiki.anidb.net/w/UDP_API_Definition#MYLIST:_Retrieve_MyList_Data
     * @param viewDate Unix time stamp of watch 0 if not watched
     * @throws AnidbException when we did not update anidb info.
     */
    public void editMyList(int lid, int state, long viewDate) throws AnidbException {
        StringBuilder sb = new StringBuilder();
        sb.append("MYLISTADD ");
        sb.append("lid=");
        sb.append(lid);
        sb.append("&state=");
        sb.append(state);
        sb.append("&viewed=");
        if (viewDate > 0) {
            sb.append("1");
            sb.append("&viewdate=");
            sb.append(viewDate);
        } else {
            sb.append("0");
        }
        sb.append("&edit=1");
        String response = sendData(sb.toString(), 210, 311);
        int code = this.getResponseCode(response);
        if (code == 311) {
            throw new AnidbException("File was deleted from anidb.info: " + response);
        }
    }

    /**
     * If we are logged this function logs us out. It ignores <b>all</b>
     * exceptions given by anidb.
     */
    public void logout() {
        if (AnidbHandler.isLoged == false) {
            return; // not loged
        }
        try {
            sendData("LOGOUT", 203, 403);
        } catch (AnidbException ignore) {
            System.out.println("Can't log out."); // we don't care
        }
        isLoged = false;
        NAT_PORT = -1;
    }

    /**
     * Sends PING with nat=1 and returns port of our system as seen by anidb.
     *
     * @return port number as seen by anidb API
     * @throws anidbjclient.lib.anidb.AnidbOfflineException when anidb is
     * offline
     */
    public int ping() throws AnidbOfflineException {
        try {
            String response = sendData("PING nat=1", 300);
            String[] tmp = response.split("\n");
            int port = Integer.parseInt(tmp[1]);
            if (AnidbHandler.offlineMode) {
                AnidbHandler.offlineMode = false;
                AnidbHandler.offlineTime = 0;
            }
            return port;
        } catch (NumberFormatException | AnidbException e) {
            throw new AnidbOfflineException("Ping mode did not pass.");
        }
    }
}
