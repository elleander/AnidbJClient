/* 
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib.anidb;

/**
 * Basic anidb Exception usually means that the data send to anidb is wrong.
 * Should be handled by checking the msg string sent to anidb by getMessage.
 *
 * @version 0.1.0, 09/19/14
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public class AnidbException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Standard anidbException.
     */
    public AnidbException() {
        super();
    }

    /**
     * Only fatal errors like 6** or 555 so we can log them or something.
     *
     * @param msg
     */
    public AnidbException(String msg) {
        super(msg);
    }

    /**
     * Internal go offline option. All exceptions should call this option and
     * not directly in case we will need to add something.
     */
    protected static void goOffline() {
        AnidbHandler.goOffline();
    }
}
