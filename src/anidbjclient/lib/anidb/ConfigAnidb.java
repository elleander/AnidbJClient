/*
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib.anidb;

import java.net.InetAddress;

/**
 * Basic interface containing anidb settings crucial to
 * {@link anidbjclient.lib.anidb.AnidbHandler}.
 *
 * @version 0.1.0, 09/19/14
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public interface ConfigAnidb {

    /**
     * Returns buffer size for anidb API, for now its 1400. but can (not) be
     * changed any time.
     *
     * @return buffer size.
     */
    int getBuffSize();

    /**
     * Returns local port which should be used for creating socket to get anidb
     * response.
     *
     * @return local port used for socket
     */
    int getLocalPort();

    /**
     * Gets anidb.net server API port.
     *
     * @return anidb API server port
     */
    int getAnidbPort();

    /**
     * Gets anidb.net API server address. This name should be compatible with
     * {@link java.net.InetAddress}.
     *
     * @return server address
     */
    InetAddress getServerAddress();

    /**
     * Name for anidb login.
     *
     * @return name of the user
     */
    String getUserName();

    /**
     * Password of user. Remember that the password should be disposed
     * (overwritten) since it can be accessed before its garbage collected, but
     * since its not a bank account working application it's not so important.
     *
     * @return
     */
    String getUserPass();

    /**
     * User API pass as given in account. This option is used only for encrypted
     * connections and is not required, if you don't support it API throw
     * {@link java.lang.UnsupportedOperationException}.
     *
     * @return user API pass
     */
    String getUserApiPass();

    /**
     * Returns version of our client. This can be only int value higher or equal
     * 1.
     *
     * @return client version according to the one given to anidb. profile
     */
    int getClientVersion();

    /**
     * Returns client name. This should be name as given to anidb in lower case.
     *
     * @return client name
     */
    String getClientName();

    /**
     * Give a file mask you want the response in FILE query. For file mask
     * setting: <a
     * href="http://wiki.anidb.net/w/UDP_API_Definition#FILE:_Retrieve_File_Data">
     * file mask</a>.
     *
     * @return string of file mask we want to use.
     */
    String getFileFMask();

    /**
     * Give anime mask we want to use in FILE query. For anime mask setting: <a
     * href="http://wiki.anidb.net/w/UDP_API_Definition#FILE:_Retrieve_File_Data">
     * anime mask</a>.
     *
     * @return anime mask we want to use.
     */
    String getFileAMask();

    /**
     * Give anime mask we want to use in ANIME query. For anime masks see: <a
     * href="http://wiki.anidb.net/w/UDP_API_Definition#ANIME:_Retrieve_Anime_Data">
     * anime mask</a>.
     *
     * @return anime mask we want to use.
     */
    String getAnimeAMask();
}
