/*
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.lib.anidb;

/**
 * Anidb exception thrown only when login/pass data are not matching the ones on
 * anidb server. Usually wrong user name/ pass or account dosn't exist, this API
 * <b>can't</b> create new account.
 *
 * @version 0.1.0, 09/19/14
 * @author Krzysztof Troska (elleander86 at gmail.com)
 */
public class AnidbWrongLoginException extends AnidbException {

    /**
     * Creates a new instance of <code>AnidbWrongLoginException</code> without
     * detail message.
     */
    public AnidbWrongLoginException() {
        super();
        AnidbException.goOffline();
    }

    /**
     * Constructs an instance of <code>AnidbWrongLoginException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public AnidbWrongLoginException(String msg) {
        super(msg);
        AnidbException.goOffline();
    }
}
