/* 
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.bin;

import anidbjclient.lib.DatabaseSQLite;
import anidbjclient.lib.Ed2kHash;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Runnable class which hashes files and inserts them into database.
 *
 * @author Krzysztof Troska <elleander86 at gmail.com>
 */
public class Ed2kCounter implements Runnable {

    private final File file;

    /**
     * Basic constructor we need file to check and database connection
     *
     * @param file file to check ed2k hash
     */
    public Ed2kCounter(File file) {
        this.file = file;
    }

    /**
     * Counts hash for the file given in constructor (one use function only),
     * and puts data into db
     */
    @Override
    public void run() {
        System.out.println("Countin ed2k hash for: " + file.getName());
        DatabaseSQLite db;
        try {
            db = new DatabaseSQLite();
        } catch (SQLException ex) {
            Logger.getLogger(Ed2kCounter.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        Ed2kHash ed2k = new Ed2kHash(file);
        try {
            db.insertFilesToAdd(file.toPath(), ed2k.getHash());
        } catch (SQLException | IOException ex) {
            Logger.getLogger(Ed2kCounter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}