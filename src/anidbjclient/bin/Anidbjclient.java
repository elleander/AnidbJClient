/* 
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.bin;

/**
 * Main function starting AnidbDaemon daemon and showing the GUI.
 * Also creates main connection used throught whole app.
 * @author Krzysztof Troska <elleander86 at gmail.com>
 */
public class Anidbjclient {
    /**
     * Main function 
     * @param args the command line arguments - for now not used
     */
    public static void main(String[] args) {
        /*Thread inserter = new Thread(new AnidbDaemon());
        inserter.setDaemon(true);
        inserter.start();*/
        AnidbJClientGUI gui = new AnidbJClientGUI();
        gui.setLocationRelativeTo(null);
        gui.setVisible(true);
    }
}