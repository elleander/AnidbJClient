/* 
 * Copyright (C) 2014 Krzysztof Troska <elleander86 at gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package anidbjclient.bin;

import anidbjclient.lib.ConfigManagerAnidbJClient;
import anidbjclient.lib.anidb.AnidbException;
import anidbjclient.lib.anidb.AnidbHandler;
import anidbjclient.lib.anidb.AnidbMissingContentException;
import anidbjclient.lib.anidb.AnidbOfflineException;
import anidbjclient.lib.ConfigManagerFile;
import anidbjclient.lib.DatabaseSQLite;
import anidbjclient.lib.Episode;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Daemon which counts ed2k sums from database and calls processNewFile on
 * them.
 *
 * @author Krzysztof Troska <elleander86 at gmail.com>
 */
public final class AnidbDaemon implements Runnable {

    private static DatabaseSQLite DB = null;
    private final ConfigManagerAnidbJClient cm = new ConfigManagerAnidbJClient(new ConfigManagerFile());
    /**
     * Interval in seconds in which we will start again
     */
    private static final int INTERVAL = 30;
    /**
     * Interval in seconds and we send ping
     */
    private static final int NAT_INTERVAL = 300 * 1000; // 3 min.
    /**
     * Time in millisec. after which we try to logout
     */
    private static final int LOGOUT_TIME = 600 * 1000; // 10 min.
    /**
     * executor used for calling Ed2kCounter
     */
    private final ExecutorService exec;
    /**
     * store this thread so garbage collector won't kill him
     */
    public static volatile List<Thread> threads = new ArrayList<>();
    public static volatile List<Path> filesToAdd = new ArrayList<>();
    private static int tries = 1;
    private static boolean canceled = false;

    public AnidbDaemon() {
        int cores = Runtime.getRuntime().availableProcessors();
        cores = (int) Math.ceil(cores / 2.0); // returns at least 1 
        exec = Executors.newFixedThreadPool(cores);
    }

    /**
     * Stops function in safe way
     */
    public static void cancel() {
        canceled = true;
    }

    /**
     * Runs in background and in set intervals sec intervals check database for
     * new files to add then sleeps again. Can be killed by using
     * {@link #cancel() cancel()} should be called after checking db connection.
     */
    @Override
    public void run() {
        if (!this.beforeStartTest()) {
            return;
        }
        threads.add(Thread.currentThread());
        try {
            /**
             * We want to delete all files which did not get ed2k somehow, so we can
             * count them again later.
             */
            DB.deleteEmptyFromFilesToAdd();
        } catch (SQLException ex) {
            Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (!canceled) {
            System.out.println("Inserter started...");
            //Starts the file walker which starts ed2k counting hash for files
            this.startFileWalker();
            if (canceled) // we want to cancel BEFORE checking DB
            {
                break;
            }
            /**
             * Get the data from files_to_add and try to insert them into db,
             * only if anidb isn't offline
             */
            if (AnidbHandler.offlineMode) {
                try {
                    this.connect();
                } catch (AnidbOfflineException ex) {
                    System.err.println("Anidb Offline: " + ex.getMessage());
                }
            }
            //check again in case we did connect
            if (!AnidbHandler.offlineMode) {
                try {
                    DB.repairDatabase();
                } catch (AnidbException | SQLException ex) {
                    Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
                    cancel();
                    return;
                }
                try {
                    this.startProcessNewFile();
                } catch (SQLException ex) {
                    Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
                    cancel();
                    return;
                }
            }
            if (canceled) {
                break;
            }
            //should we logout?
            this.testLogout();
            this.testPing();
            System.out.println("Slepping...");
            try {
                TimeUnit.SECONDS.sleep(INTERVAL);
            } catch (InterruptedException ex) {
                /**
                 * We get interrupt signal call for interrupted sets it to false
                 * we *generally* don't need it thought.
                 */
                Thread.interrupted();
            }
        }
        System.err.println("AnidbInserter ended");
    }

    /**
     * Starts file walker for path set in ConfigManagerFile. It's not started if
     * Auto_Scan is set to false.
     */
    private void startFileWalker() {
        if (!cm.autoScan())
            return;
        Path path = cm.getDownloadPath();
        if (path.toFile().isDirectory()) {
            try {
                Files.walkFileTree(path, new FileWalker(this));
            } catch (IOException e) {
                System.err.println("Error in calling walkFileTree.");
            }
        } else {
            System.err.println("Wrong path given in config file.");
        }
    }

    /**
     * Test if we are logged to anidb and if time passed from last send packet is
     * grater than {@link #LOGOUT_TIME LOGOUT_TIME} if so we try to logout.
     */
    private void testLogout() {
        if (AnidbHandler.isLoged && ((System.currentTimeMillis()
                - AnidbHandler.sendTime) > LOGOUT_TIME)) {
            AnidbHandler ah = new AnidbHandler(cm);
            ah.logout();
        }
    }

    /**
     * Starts process new file for selected files in files_to_add db.
     */
    private void startProcessNewFile() throws SQLException {
        final String[] columns = {"ed2k", "path", "fid"};
        final String database = "files_to_add";
        Map<String, List<Object>> map;
        map = DB.getDatabaseInfo(columns, database);
        int size = map.get("ed2k").size(); // randdom column just to get info how many rows we selected
        for (int i = 0; i < size; i++) {
            Path path = Paths.get((String) map.get("path").get(i));
            String hash = (String) map.get("ed2k").get(i);
            int fid = (int) map.get("fid").get(i);
            if (!path.toFile().isFile()) {
                try {
                    DB.deleteFromFilesToAdd(path);
                } catch (SQLException ex) { // we don't realy care
                    Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (fid > 0) // if we got fid we want only to move the file
            {
                Episode.moveFile(path, fid);
            } else if (map.get("ed2k").get(i) != "0") { // file exists is just not added to db.
                try {
                    fid = DB.processNewFile(path, hash);
                    if (fid != -1) {
                        if (Episode.moveFile(path, fid)) {
                            DB.deleteFromFilesToAdd(hash);
                        } else // we can't move file (prodably is opened)
                        {
                            DB.insertFilesToAdd(path, hash, fid);
                        }
                    }
                } catch (AnidbMissingContentException e) {
                    try {
                        DB.deleteFromFilesToAdd(hash);
                        DB.insertOtherFiles(path, hash);
                    } catch (SQLException ex) {
                        Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
                        return;
                    }
                }  catch (AnidbException | SQLException ex) {
                    Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
            }
        }
    }

    /**
     * Iterates throught filesToAdd if there is a directory call File walker if
     * there is file call {@link #insertFile(java.nio.file.Path) insertFile()}
     */
    public void checkFilesToAdd() {
        int size = filesToAdd.size();
        try {
            for (int i = 0; i < size; i++) {
                Path path = filesToAdd.get(i);
                /* Directory starting file walker */
                if (path.toFile().isDirectory()) {
                    try {
                        Files.walkFileTree(path, new FileWalker(this));
                    } catch (IOException e) {
                        System.err.println("Error in calling walkFileTree.");
                    }
                } else if (path.toFile().isFile()) {
                    this.insertFile(path);
                }
                filesToAdd.remove(i);
                if (AnidbDaemon.canceled) {
                    return;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Calls Ed2kCounter on a given file.
     *
     * @param path to a file
     * @throws java.sql.SQLException
     */
    public void insertFile(Path path) throws SQLException {
        if (DB.checkFilesToAdd(path)) {
            return; // if file was alredy checked we move on
        }
        if (Episode.checkFile(path, false)) {
            DB.insertFilesToAdd(path); // insert into files_to_add so we know that file is already being counted.
            exec.execute(new Ed2kCounter(path.toFile()));
        }
    }

    /**
     * Try to connect to anidb and remove offline status if can't we throw
     * exception.
     *
     * @throws AnidbOfflineException in case we could't connect.
     */
    private void connect() throws AnidbOfflineException {
        int offlineTime = (int) ((System.currentTimeMillis()
                - AnidbHandler.offlineTime) / 1000); //how long offline in sec.
        //connect again if we are offline more than 2 min * sqrt(tries)
        //e.g. after 2 min, 8 min, 18 min etc.
        if (offlineTime > (120 * Math.sqrt(tries))) {
            AnidbHandler ah = new AnidbHandler(cm);
            if (tries++ > 5) { // we are already waiting long enaught
                tries = 1;
            }
            ah.ping();// connects or throws anidbOffline exception
        }
    }

    /**
     * Tests if we should ping the server (if there is nat at work and time set
     * in NAT_INTERVAL is longer than last response.
     */
    private void testPing() {
        /*
         * if there is no NAT flag set or time for last send is smaller than
         * NAT_INTERVAL then we ignore this.
         */
        if (!AnidbHandler.isLoged || (AnidbHandler.NAT_PORT == -1)
                || ((System.currentTimeMillis() - AnidbHandler.sendTime) < NAT_INTERVAL)) {
            return;
        }
        AnidbHandler ah = new AnidbHandler(cm);
        try {
            ah.ping();
        } catch (AnidbOfflineException ex) {
            Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Initial check of settings, if its safe to run daemon.
     * @return true if all is ok, false if we found error and daemon should not
     * be started.
     */
    private boolean beforeStartTest() {
        if (DB == null)
            try {
                DB = new DatabaseSQLite();
            } catch (SQLException ex) {
                return false;
            }
        for (int i = 0; i < threads.size(); i++) {
            if (threads.get(i).isAlive()) {
                System.err.println("One daemon is still alive, can't start another one");
                threads.get(i).interrupt(); // interupt it so it starts again if needed.
                return false; // don't start another thread if one is alive.
            } else {
                threads.remove(i); //removes old threads so they can be disposed
            }
        }
        return true;
    }

    /**
     * Basic file walker.
     */
    private static class FileWalker implements FileVisitor<Path> {

        ConfigManagerFile cm = new ConfigManagerFile();
        private final AnidbDaemon AI;

        /**
         * Creates basic classes we will need and sets the number of executor
         * services we will use.
         */
        FileWalker(AnidbDaemon ai) {
            this.AI = ai;
        }

        @Override
        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
            if (AnidbDaemon.canceled) {
                return FileVisitResult.TERMINATE;
            }
            try {
                AI.insertFile(path);
            } catch (SQLException ex) {
                Logger.getLogger(AnidbDaemon.class.getName()).log(Level.SEVERE, null, ex);
                return FileVisitResult.TERMINATE;
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir,
                BasicFileAttributes attrs) {
            //For visited directory
            //System.out.println("Found Directory: " + dir.getName(0));
            if (AnidbDaemon.canceled) {
                return FileVisitResult.TERMINATE;
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file,
                IOException exc) {
            exc.printStackTrace(System.err);
            if (AnidbDaemon.canceled) {
                return FileVisitResult.TERMINATE;
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            if (AnidbDaemon.canceled) {
                return FileVisitResult.TERMINATE;
            }
            if (dir == cm.getDownloadPath()) {
                return FileVisitResult.CONTINUE;
            }
            try {
                Files.delete(dir);
            } catch (DirectoryNotEmptyException | AccessDeniedException e) {
            }
            return FileVisitResult.CONTINUE;
        }
    }
}